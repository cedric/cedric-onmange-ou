#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
from refuge import app

try:
    # if the application is deployed on Google AppEngine
    import google.appengine
except:
    # if the application is deployed on Heroku
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
